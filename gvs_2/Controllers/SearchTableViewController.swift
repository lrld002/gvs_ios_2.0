//
//  SearchViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 26/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Nuke

class SearchTableViewController: UITableViewController, UISearchBarDelegate, UISearchResultsUpdating {
    
    @IBOutlet var searchTableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    var medicinesArray = [NSDictionary?]()
    var filteredMedicines = [NSDictionary?]()
    var databaseRef = Database.database().reference()
    
    var searchMID: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
        
        databaseRef.child(AppConstants.FIREBASE_DATABASE_TABLE_ITEMS).queryOrdered(byChild: "name").observe(.childAdded, with: {(snapshot) in
            
            self.medicinesArray.append(snapshot.value as?  NSDictionary)
            //self.searchTableView.insertRows(at: [IndexPath(row: self.medicinesArray.count-1, section: 0)], with: .automatic)
            
        }) {(error) in
            print(error.localizedDescription)
        }
    }
    
    private func setUpViews() {
        definesPresentationContext = true
        
        searchController.searchBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        searchController.searchBar.tintColor = UIColor(named: "mainBlueColor")
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.showsCancelButton = true
        searchController.searchBar.delegate = self
        //searchController.searchBar.placeholder = NSLocalizedString("Search", comment: "")
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.hidesBackButton = true
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredMedicines.count
        }
        return self.medicinesArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "search_cell", for: indexPath)
        
        let medicine: NSDictionary?
        
        if searchController.isActive && searchController.searchBar.text != "" {
            medicine = filteredMedicines[indexPath.row]
        } else {
            medicine = self.medicinesArray[indexPath.row]
        }
        
        cell.textLabel?.text = medicine?["name"] as? String
        cell.textLabel?.numberOfLines = 3
        cell.detailTextLabel?.isHidden = true
        cell.selectionStyle = .none
        
        let url = URL.init(string: medicine?["images"] as! String)
        if (url != nil && !url!.absoluteString.isEmpty) {
            Nuke.loadImage(with: url!, into: (cell.imageView)!)
        } else {
            Nuke.loadImage(with: URL(string: AppConstants.GVS_DEFAULT_PROFILE_IMAGE_URL)!, into: (cell.imageView)!)
        }
        
        cell.addSubview(cell.imageView!)
        cell.addConstraintWithVisualFormat(format: "H:|-8-[v0(70)]", views: cell.imageView!)
        cell.addConstraintWithVisualFormat(format: "V:|-8-[v0(70)]-8-|", views: cell.imageView!)
        
        cell.addSubview(cell.textLabel!)
        cell.addConstraintWithVisualFormat(format: "H:|-120-[v0]-8-|", views: cell.textLabel!)
        cell.addConstraintWithVisualFormat(format: "V:|-8-[v0]", views: cell.textLabel!)
        
        /*cell.addSubview(cell.detailTextLabel!)
        cell.addConstraintWithVisualFormat(format: "H:|-88-[v0]-8-|", views: cell.detailTextLabel!)
        cell.addConstraintWithVisualFormat(format: "V:|-32-[v0]-8-|", views: cell.detailTextLabel!)*/
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let medicine: NSDictionary?
        if searchController.isActive && searchController.searchBar.text != "" {
            medicine = filteredMedicines[indexPath.row]
        } else {
            medicine = self.medicinesArray[indexPath.row]
        }
        searchMID = medicine?["id"] as? String
        self.performSegue(withIdentifier: "search_medicine", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "search_medicine") {
            let goMedicine = segue.destination as! MedicineViewController
            goMedicine.currentMID = self.searchMID
            
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContent(searchText: self.searchController.searchBar.text!)
    }
    
    func filterContent(searchText: String) {
        self.filteredMedicines = self.medicinesArray.filter { medicine in
            let medicineName = medicine!["name"] as? String
            return(medicineName?.lowercased().contains(searchText.lowercased()))!
        }
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationController?.popViewController(animated: true)
    }

}
