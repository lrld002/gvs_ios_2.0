//
//  AddPaymentViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 30/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import Firebase
import Openpay
import CountryPicker
import FirebaseFunctions

class AddPaymentViewController: UIViewController {

    @IBOutlet weak var addCardButton: UIButton!
    @IBOutlet weak var addCashButton: UIButton!
    
    var openpay: Openpay!
    var sessionId: String = ""
    var addressDictionary = Dictionary<String, Any>()
    private var addressViewController: UIViewController!
    lazy var functions = Functions.functions()
    
    private enum indexTag: Int {
        case line1 = 1
        case line2 = 2
        case line3 = 3
        case postalCode = 4
        case city = 5
        case state = 6
        case country = 7
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        openpay = Openpay(withMerchantId: AppConstants.OPENPAY_MERCHANT_ID, andApiKey: AppConstants.OPENPAY_PUBLIC_KEY, isProductionMode: false, isDebug: true)

        setUpViews()
        setUpConstraints()
    }
    

    private func setUpViews() {
        self.title = NSLocalizedString("Add payment", comment: "")
        
        addCardButton.setImage(#imageLiteral(resourceName: "Card"), for: .normal)
        addCardButton.setTitle(NSLocalizedString("Add credit or debit card", comment: ""), for: .normal)
        addCardButton.tintColor = UIColor(named: "mainBlueColor")
        addCardButton.contentHorizontalAlignment = .center
        addCardButton.contentVerticalAlignment = .center
        addCardButton.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        addCardButton.layer.borderWidth = 1
        let cardGesture = UITapGestureRecognizer(target: self, action: #selector(addCardClicked))
        addCardButton.addGestureRecognizer(cardGesture)
        
        addCashButton.setImage(#imageLiteral(resourceName: "Coin"), for: .normal)
        addCashButton.setTitle(NSLocalizedString("Cash", comment: ""), for: .normal)
        addCashButton.tintColor = UIColor(named: "mainBlueColor")
        addCashButton.contentHorizontalAlignment = .center
        addCashButton.contentVerticalAlignment = .center
        addCashButton.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        addCashButton.layer.borderWidth = 1
    }
    
    private func setUpConstraints() {
        
        view.addSubview(addCardButton)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: addCardButton)
        view.addConstraintWithVisualFormat(format: "V:|-120-[v0(48)]", views: addCardButton)
        
        view.addSubview(addCashButton)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: addCashButton)
        view.addConstraintWithVisualFormat(format: "V:|-160-[v0(48)]", views: addCashButton)
    }
    
    @objc func addCardClicked() {
        if AppDelegate.currentUser.payId.isEmpty {
            let params = ["name": AppDelegate.currentUser.name.unsafelyUnwrapped,
                          "email": AppDelegate.currentUser.mail.unsafelyUnwrapped,
                          "requires_account": false] as [String : Any]
            functions.httpsCallable(GVSFirebaseFunctions.NEW_CLIENT).call(params) { (result, error) in
                if let error = error as NSError? {
                    if error.domain == FunctionsErrorDomain {
                        //let code = FunctionsErrorCode(rawValue: error.code)
                        //let message = error.localizedDescription
                        //let details = error.userInfo[FunctionsErrorDetailsKey]
                    }
                    // ...
                }
                if let text = (result?.data as? [String: Any]){
                    print(text)
                    do {
                        AppDelegate.currentUser.payId = text["id"] as? String
                        AppDelegate.publishUserChanges()
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            }
        }else {
            createDeviceSession()
        }
        
    }
    
    func createDeviceSession() {
        openpay.createDeviceSessionId(successFunction: showCardView, failureFunction: failure)
    }
    
    func showCardView(sessionID: String){
        sessionId = sessionID
        openpay.loadCardForm(in: self, successFunction: addAddress, failureFunction: failCard, formTitle: "Add card")
    }
    
    func failure(error: NSError){
        
    }
    
    func addAddress() {
        let addressView = AddressView.instanceFromNib()
        addressView.translatesAutoresizingMaskIntoConstraints = false
        addressViewController = UIViewController()
        addressViewController.view.addSubview(addressView)
        addressViewController.hideKeyboardWhenTappedAround()
        addressViewController.view.addConstraintWithVisualFormat(format: "H:|-0-[v0]-0-|", views: addressView)
        addressViewController.view.addConstraintWithVisualFormat(format: "V:|-0-[v0]-0-|", views: addressView)
        if let countryPicker = addressViewController.view.viewWithTag(indexTag.country.rawValue) as? CountryPicker {
            let locale = Locale.current
            let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
            countryPicker.setCountry(code ?? "")
        }
        let nav = UINavigationController(rootViewController: addressViewController)
        
        if let topItem = nav.navigationBar.topItem {
            topItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("button.cancel", bundle: Bundle(for: Openpay.self), comment: "Cancel"),
                                                        style: .plain,
                                                        target: self,
                                                        action: #selector(cancelCardCreation))
            topItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("button.continue", bundle: Bundle(for: Openpay.self), comment: "Continue"),
                                                         style: .done,
                                                         target: self,
                                                         action: #selector(createCardToken))
            
            topItem.rightBarButtonItem?.isEnabled = true
        }
        DispatchQueue.main.async() {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    func failCard(error: NSError) {
        print("Fail Card Capture...")
        print("\(error.code) - \(error.localizedDescription)")
    }
    
    @objc func cancelCardCreation() {
        self.addressViewController.dismiss(animated: true, completion: nil);
    }
    
    @objc func createCardToken() {
        var line1 = ""
        var line2 = ""
        var line3 = ""
        var postalCode = ""
        var city = ""
        var state = ""
        var countryCode = ""
        
        if let uiLine1 = self.addressViewController.view.viewWithTag(indexTag.line1.rawValue) as? UITextField {
            line1 = uiLine1.text ?? ""
        }
        
        if let uiLine2 = self.addressViewController.view.viewWithTag(indexTag.line2.rawValue) as? UITextField {
            line2 = uiLine2.text ?? ""
        }
        
        if let uiLine3 = self.addressViewController.view.viewWithTag(indexTag.line3.rawValue) as? UITextField {
            line3 = uiLine3.text ?? ""
        }
        
        if let uiPostalCode = self.addressViewController.view.viewWithTag(indexTag.postalCode.rawValue) as? UITextField {
            postalCode = uiPostalCode.text ?? ""
        }
        
        if let uiCity = self.addressViewController.view.viewWithTag(indexTag.city.rawValue) as? UITextField {
            city = uiCity.text ?? ""
        }
        
        if let uistate = self.addressViewController.view.viewWithTag(indexTag.state.rawValue) as? UITextField {
            state = uistate.text ?? ""
        }
        
        if let uiCountryPicker = self.addressViewController.view.viewWithTag(indexTag.country.rawValue) as? CountryPicker {
            countryCode = uiCountryPicker.currentCountry?.code ?? ""
        }
        
        addressDictionary = [
            "postal_code":postalCode,
            "line1":line1,
            "line2":line2,
            "line3":line3,
            "city":city,
            "state":state,
            "country_code":countryCode
        ]
        openpay.createTokenWithCard(address: OPAddress(with: addressDictionary), successFunction: successToken, failureFunction: failToken)
        self.addressViewController.dismiss(animated: true, completion: nil)
    }
    
    func successToken(token: OPToken) {
        print("Success Token...")
        print("TokenID: \(token.id)")
        let params = ["customer_id": AppDelegate.currentUser.payId.unsafelyUnwrapped,
                      "device_session_id": sessionId,
                      "token_id": token.id] as [String : Any]
        
        print(params)
        functions.httpsCallable(GVSFirebaseFunctions.NEW_CARD).call(params) { (result, error) in
            if let error = error as NSError? {
                
            }
            if let response = (result?.data as? [String: Any]) {
                print(response)
                let card = try! JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let openpayCard = try! JSONDecoder().decode(OpenpayCard.self, from: card)
                AppDelegate.currentUser.cards.append(openpayCard)
                AppDelegate.publishUserChanges()
            }
        }
        
        /*networking.put(GVSOpenpayAPIEndpoints.CARDS_NEW,
         parameterType: .formURLEncoded,
         parameters: params
         ) { result in
         switch result {
         case .success(let response):
         do {
         let openpayCard = try JSONDecoder().decode(OpenpayCard.self, from: response.data)
         AppDelegate.currentUser.cards.append(openpayCard)
         AppDelegate.publishUserChanges()
         } catch {
         //Desplegar mensaje de error
         }
         case .failure(let error):
         print(error)
         }
         }*/
    }
    
    func failToken(error: NSError) {
        print("Fail Token...")
        print("\(error.code) - \(error.localizedDescription)")
        DispatchQueue.main.sync() {
            //showBlackBox(show: false)
            //if let resultController = storyboard!.instantiateViewController(withIdentifier: "ResultController") as? ResultViewController {
            //present(resultController, animated: true, completion: nil)
            //resultController.textBox.text = String(format: NSLocalizedString("token.error", bundle: Bundle.main, comment: "Error JSON"), error.localizedDescription)
            //}
        }
        
    }

}
