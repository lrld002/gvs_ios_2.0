//
//  ViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 26/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import Firebase
import Nuke

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    var selectedMID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
        setUpConstraints()
    }
    
    private func setUpViews() {
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "Cart"), for: .normal)
        //button.setTitle(String(AppDelegate.currentCart.items!.count), for: .normal)
        button.sizeToFit()
        button.addTarget(self, action: #selector(goToCart), for: .touchUpInside)

        let cartButton = UIBarButtonItem(customView: button)
        cartButton.tintColor = UIColor(named: "mainBlueColor")
        
        let accountButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Account"), style: .done, target: self, action: #selector(goToAccount))
        accountButton.tintColor = UIColor(named: "mainBlueColor")
        
        let searchButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Search"), style: .done, target: self, action: #selector(goToSearch))
        searchButton.tintColor = UIColor(named: "mainBlueColor")
        
        self.navigationItem.rightBarButtonItems = [accountButton, cartButton, searchButton]
        
        mainCollectionView.delegate = self
        //mainCollectionView.backgroundColor = UIColor(named: "mainBlueColor")
        
        let nib = UINib.init(nibName: "medicineCollectionViewCell", bundle: nil)
        mainCollectionView.register(nib, forCellWithReuseIdentifier: "collection_medicine_cell")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: view.frame.width/3, height: view.frame.width/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        mainCollectionView!.collectionViewLayout = layout
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
    
    }
    
    private func setUpConstraints() {
        view.addSubview(mainCollectionView)
        view.addConstraintWithVisualFormat(format: "H:|-0-[v0]-0-|", views: mainCollectionView)
        view.addConstraintWithVisualFormat(format: "V:|-0-[v0]-0-|", views: mainCollectionView)
    }
    
    @objc func goToCart() {
        self.performSegue(withIdentifier: "show_cart_main", sender: self)
    }
    
    @objc func goToAccount() {
        if (Auth.auth().currentUser?.isAnonymous)! {
            self.performSegue(withIdentifier: "show_login_main", sender: self)
        } else {
            self.performSegue(withIdentifier: "show_account_main", sender: self)
        }
    }
    
    @objc func goToSearch() {
        self.performSegue(withIdentifier: "show_search_main", sender: self)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_medicine_cell", for: indexPath) as? MedicineCollectionViewCell {
                        
            cell.medicineName.isHidden = true
            cell.medicineTag.isHidden = true
            cell.medicineView.isHidden = true
            
            cell.medicineImage.contentMode = .scaleAspectFill
            cell.medicineImage.clipsToBounds = true
            cell.medicineImage.layer.cornerRadius = 0.1 * cell.medicineImage.frame.width
            cell.medicineImage.layer.borderWidth = 1
            
            cell.addSubview(cell.medicineImage)
            cell.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: cell.medicineImage)
            cell.addConstraintWithVisualFormat(format: "V:|-[v0]-|", views: cell.medicineImage)
            
            cell.addSubview(cell.medicineName)
            cell.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: cell.medicineName)
            cell.addConstraintWithVisualFormat(format: "V:|-[v0]-|", views: cell.medicineName)
            
            if indexPath.row == 0 {
                //Nuke.loadImage(with: URL.init(string: AppConstants.GVS_DEFAULT_PROFILE_IMAGE_URL)!, into: cell.medicineImage)
                Nuke.loadImage(with: URL.init(string: "https://firebasestorage.googleapis.com/v0/b/farmacia-gvs.appspot.com/o/static%2Fmedicamentos.jpg?alt=media&token=44c1aa7b-596a-44bc-991f-b0d6434ec7b8")!, into: cell.medicineImage)
                cell.medicineImage.contentMode = .scaleAspectFit
            }
            if indexPath.row == 1 {
                Nuke.loadImage(with: URL.init(string: "https://firebasestorage.googleapis.com/v0/b/farmacia-gvs.appspot.com/o/static%2Fmedicamentos.jpg?alt=media&token=44c1aa7b-596a-44bc-991f-b0d6434ec7b8")!, into: cell.medicineImage)
                cell.medicineName.isHidden = false
                cell.medicineName.text = "Lunes 10% en medicamentos"
                cell.medicineName.textColor = UIColor.lightGray
                cell.medicineName.numberOfLines = 5
                cell.medicineName.textAlignment = .center
                cell.medicineName.font = cell.medicineName.font.withSize(28)
            }
            if indexPath.row == 2 {
                Nuke.loadImage(with: URL.init(string: "https://firebasestorage.googleapis.com/v0/b/farmacia-gvs.appspot.com/o/static%2Fsilladeruedas.jpg?alt=media&token=90c3e503-534b-4632-bd5e-2740ae0306ec")!, into: cell.medicineImage)
                cell.medicineName.isHidden = false
                cell.medicineName.text = "Aparatos ortopédicos"
                cell.medicineName.textColor = UIColor.lightGray
                cell.medicineName.numberOfLines = 5
                cell.medicineName.textAlignment = .center
                cell.medicineName.font = cell.medicineName.font.withSize(28)
            }
            
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width/2, height: view.frame.width)
    }
    
    /*override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "show_medicine_main") {
            let goMedicine = segue.destination as! MedicineViewController
            goMedicine.currentMID = self.selectedMID
        }
    }*/
    
    /*func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = Item(dictionary: collectionViewDataSource.items[indexPath.row].data())
        selectedMID = item.id
        performSegue(withIdentifier: "show_medicine_main", sender: self)
    }*/

}
