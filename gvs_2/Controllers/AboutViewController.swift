//
//  AboutViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 13/02/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var aboutFacebook: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
        setUpConstraints()
    }
    
    private func setUpViews() {
        self.title = "Quiénes somos"
        
        aboutFacebook.setTitle("Siguenos en Facebook", for: .normal)
        aboutFacebook.tintColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        aboutFacebook.contentHorizontalAlignment = .center
        aboutFacebook.contentVerticalAlignment = .center
        aboutFacebook.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        aboutFacebook.layer.borderWidth = 1
        
    }
    
    private func setUpConstraints() {
        view.addSubview(aboutFacebook)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: aboutFacebook)
        view.addConstraintWithVisualFormat(format: "V:|-160-[v0(48)]|", views: aboutFacebook)
    }
    
    @IBAction func openFacebookPage(_ sender: Any) {
        if let url = URL(string: "https://facebook.com/farmaciasgvs") {
            UIApplication.shared.open(url, options: [:])
        }
    }

}
