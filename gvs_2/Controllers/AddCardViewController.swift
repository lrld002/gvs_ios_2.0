//
//  AddCardViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 30/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import CountryPicker

class AddCardViewController: UIViewController, UITextFieldDelegate, CountryPickerDelegate {
    
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cardDateLabel: UILabel!
    @IBOutlet weak var cardCVVLabel: UILabel!
    @IBOutlet weak var cardCountryLabel: UILabel!
    
    @IBOutlet weak var addCardNumber: UITextField!
    @IBOutlet weak var addCardDate: UITextField!
    @IBOutlet weak var addCardCVV: UITextField!
    @IBOutlet weak var addCardCountry: CountryPicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
        setUpConstraints()
        
        //get current country
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        
        //addCardCountry.displayOnlyCountriesWithCodes = ["DK", "SE", "NO", "DE"] //display only
        //addCardCountry.exeptCountriesWithCodes = ["RU"] //exept country
        //let theme = CountryViewTheme(countryCodeTextColor: .white, countryNameTextColor: .white, rowBackgroundColor: .black, showFlagsBorder: false)        //optional for UIPickerView theme changes
        //addCardCountry.theme = theme //optional for UIPickerView theme changes
        addCardCountry.countryPickerDelegate = self
        //addCardCountry.showPhoneNumbers = true
        addCardCountry.setCountry(code!)
    }
    
    private func setUpViews() {
        self.title = "Agregar tarjeta"
        
        cardNumberLabel.text = "Número de tarjeta"
        cardDateLabel.text = "Fecha de vto."
        cardCVVLabel.text = "CVV"
        cardCountryLabel.text = "País"
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        
        addCardNumber.inputAccessoryView = keyboardToolbar
        addCardNumber.keyboardType = UIKeyboardType.numberPad
        addCardNumber.delegate = self
        
        addCardDate.inputAccessoryView = keyboardToolbar
        addCardDate.keyboardType = UIKeyboardType.numberPad
        addCardDate.delegate = self
        addCardDate.placeholder = "MM/AA"
        
        addCardCVV.inputAccessoryView = keyboardToolbar
        addCardCVV.keyboardType = UIKeyboardType.numberPad
        addCardCVV.delegate = self
        addCardCVV.placeholder = "123"
    }
    
    private func setUpConstraints() {
        
        view.addSubview(cardNumberLabel)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]|", views: cardNumberLabel)
        view.addConstraintWithVisualFormat(format: "V:|-80-[v0]", views: cardNumberLabel)
        
        view.addSubview(addCardNumber)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: addCardNumber)
        view.addConstraintWithVisualFormat(format: "V:|-100-[v0]", views: addCardNumber)
        
        view.addSubview(cardDateLabel)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-240-|", views: cardDateLabel)
        view.addConstraintWithVisualFormat(format: "V:|-220-[v0]", views: cardDateLabel)
        
        view.addSubview(addCardDate)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-240-|", views: addCardDate)
        view.addConstraintWithVisualFormat(format: "V:|-240-[v0]", views: addCardDate)
        
        view.addSubview(cardCVVLabel)
        view.addConstraintWithVisualFormat(format: "H:|-240-[v0]|", views: cardCVVLabel)
        view.addConstraintWithVisualFormat(format: "V:|-220-[v0]", views: cardCVVLabel)
        
        view.addSubview(addCardCVV)
        view.addConstraintWithVisualFormat(format: "H:|-240-[v0]-|", views: addCardCVV)
        view.addConstraintWithVisualFormat(format: "V:|-240-[v0]", views: addCardCVV)
        
        view.addSubview(cardCountryLabel)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: cardCountryLabel)
        view.addConstraintWithVisualFormat(format: "V:|-340-[v0]", views: cardCountryLabel)
        
        view.addSubview(addCardCountry)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: addCardCountry)
        view.addConstraintWithVisualFormat(format: "V:|-360-[v0]", views: addCardCountry)
    }
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        cardCountryLabel.text = name
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var maxLength: Int = 20
        var currentString: NSString = textField.text! as NSString
        var newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if(addCardNumber.isEditing) {
            maxLength = 28
            currentString = textField.text! as NSString
            newString = currentString.replacingCharacters(in: range, with: string) as NSString
            
            var originalText = textField.text
            if range.location == 4 || range.location == 11 || range.location == 18 {
                originalText?.append(" - ")
                textField.text = originalText
            }
            
        } else if (addCardCVV.isEditing) {
            maxLength = 3
            currentString = textField.text! as NSString
            newString = currentString.replacingCharacters(in: range, with: string) as NSString
            
        } else if (addCardDate.isEditing) {
            if range.length > 0 {
                return true
            }
            if string == "" {
                return false
            }
            if range.location == 1 && (string.contains("3") || string.contains("4") || string.contains("5") || string.contains("6") || string.contains("7") || string.contains("8") || string.contains("9")) {
                return false
            }
            
            if range.location > 4 {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            //Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            
            //Put / after 2 digit
            if range.location == 2 {
                originalText?.append("/")
                textField.text = originalText
            }
            print("LA FECHA ES \(addCardDate.text!)")
            return true
        }
        return newString.length <= maxLength
    }
    
    @objc override func dismissKeyboard() {
        self.view.endEditing(true)
    }

}
