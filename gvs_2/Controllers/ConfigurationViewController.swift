//
//  ConfigurationViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 30/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit

class ConfigurationViewController: UIViewController {
    
    @IBOutlet weak var configurationName: UILabel!
    @IBOutlet weak var configurationMail: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpviews()
        setUpConstraints()
    }
    
    private func setUpviews() {
        self.title = NSLocalizedString("Configuration", comment: "")
        
        configurationName.text = "\(NSLocalizedString("Name", comment: "")) : \n\(String(AppDelegate.currentUser.name))"
        configurationName.numberOfLines = 2
        configurationName.textColor = UIColor(named: "mainBlueColor")
        configurationName.textAlignment = .center
        configurationName.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        configurationName.layer.borderWidth = 1
        
        configurationMail.text = "\(NSLocalizedString("Email", comment: "")) : \n\(String(AppDelegate.currentUser.mail))"
        configurationMail.numberOfLines = 2
        configurationMail.textColor = UIColor(named: "mainBlueColor")
        configurationMail.textAlignment = .center
        configurationMail.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        configurationMail.layer.borderWidth = 1
        
        logoutButton.isHidden = true
        let logoutGesture = UITapGestureRecognizer(target: self, action: #selector(logOutClicked))
        logoutButton.addGestureRecognizer(logoutGesture)
        logoutButton.setTitle("Cerrar sesión", for: .normal)
        logoutButton.tintColor = UIColor.white
        logoutButton.backgroundColor = UIColor(named: "mainBlueColor")
    }
    
    private func setUpConstraints() {
        
        view.addSubview(configurationName)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: configurationName)
        view.addConstraintWithVisualFormat(format: "V:|-48-[v0]", views: configurationName)
        
        view.addSubview(configurationMail)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: configurationMail)
        view.addConstraintWithVisualFormat(format: "V:|-120-[v0]", views: configurationMail)
        
        view.addSubview(logoutButton)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: logoutButton)
        view.addConstraintWithVisualFormat(format: "V:|-240-[v0]", views: logoutButton)

    }
    
    @objc func logOutClicked(){
        let logOutAlert = UIAlertController(title: NSLocalizedString("Log out", comment: ""), message: NSLocalizedString("You are about to log out", comment: ""), preferredStyle: .alert)
        let logOutAction = UIAlertAction(title: NSLocalizedString("Log out", comment: ""), style: .destructive) { (action) in
            self.dismiss(animated: true, completion: nil)
            try! Auth.auth().signOut()
            FBSDKLoginManager().logOut()
            self.shouldPerformSegue(withIdentifier: "show_configuration_logout", sender: self)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        logOutAlert.addAction(logOutAction)
        logOutAlert.addAction(cancelAction)
        present(logOutAlert, animated: true, completion: nil)
    }

}
