//
//  LocationViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 31/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class LocationViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var locationMap: MKMapView!
    
    var locationManager = CLLocationManager()
    var totalArticles: String!
    var totalPayment: Double!
    var position = ""
    
    var sendLocationButton: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // Check for Location Services
        if (CLLocationManager.locationServicesEnabled()) {
            //locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
        
        //Zoom to user location
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 200, longitudinalMeters: 200)
            locationMap.setRegion(viewRegion, animated: false)
        }
        
        self.locationManager = locationManager
        
        DispatchQueue.main.async {
            self.locationManager.startUpdatingLocation()
        }

        setUpViews()
        setUpConstraints()
        getAddressFromLatLon(Latitude: locationMap.centerCoordinate.latitude, Longitude: locationMap.centerCoordinate.longitude)
    }
    
    private func setUpViews() {
        self.title = NSLocalizedString("Add location", comment: "")
        
        locationMap.showsUserLocation = true
        locationMap.delegate = self
        
        //let sendLocationButton = UIBarButtonItem(title: "enviar", style: .done, target: self, action:#selector(sendLocation))
        sendLocationButton = UIBarButtonItem(title: NSLocalizedString("Send", comment: ""), style: .done, target: self, action:#selector(sendLocation))
        sendLocationButton!.isEnabled = false
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(enableButton), userInfo: nil, repeats: false)
        
        self.navigationItem.rightBarButtonItem = sendLocationButton
    }
    
    private func setUpConstraints() {
        view.addSubview(locationMap)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: locationMap)
        view.addConstraintWithVisualFormat(format: "V:|[v0]|", views: locationMap)
    }
    
    func mapViewDidFailLoadingMap(_ mapView: MKMapView, withError error: Error) {
        print("error")
    }
    
    @objc func enableButton() {
        sendLocationButton!.isEnabled = true
    }
    
    @objc func sendLocation() {
        getAddressFromLatLon(Latitude: locationMap.centerCoordinate.latitude, Longitude: locationMap.centerCoordinate.longitude)
        print("Ubicación en = \(position)")
        self.performSegue(withIdentifier: "show_map_resume", sender: self)
    }
    
    func getAddressFromLatLon(Latitude: Double, Longitude: Double) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = Latitude
        center.longitude = Longitude
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        var addressString : String = ""
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country as Any)
                    print(pm.locality as Any)
                    print(pm.subLocality as Any)
                    print(pm.subThoroughfare as Any)
                    print(pm.thoroughfare as Any)
                    print(pm.postalCode as Any)
                    print(pm.subThoroughfare as Any)
                    //var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + " "
                    }
                    if pm.subThoroughfare != nil {
                        addressString = addressString + pm.subThoroughfare! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    print(addressString)
                    self.position = addressString
                }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "show_map_resume") {
            let goResume = segue.destination as! ResumeViewController
            
            goResume.selectedPosition = self.position
            goResume.totalArticles = self.totalArticles
            goResume.totalPayment = self.totalPayment
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error::: \(error)")
        locationManager.stopUpdatingLocation()
        let alert = UIAlertController(title: NSLocalizedString("Configuration", comment: ""), message: NSLocalizedString("This is required to get current location to send the product.", comment: ""), preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title:  NSLocalizedString("Open configuration", comment: ""), style: .default, handler: { action in
            switch action.style{
            case .default: UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            case .cancel: print("cancel")
            case .destructive: print("destructive")

            }
        }))
    }

}
