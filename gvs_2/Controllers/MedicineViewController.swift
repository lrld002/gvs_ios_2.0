//
//  MedicineViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 26/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import Firebase
import Nuke

extension UIViewController {
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 125, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

class MedicineViewController: UIViewController {
    
    @IBOutlet weak var profileMedicineImage: UIImageView!
    @IBOutlet weak var profileMedicineName: UILabel!
    @IBOutlet weak var profileMedicineDescription: UITextView!
    @IBOutlet weak var profileMedicineTag: UILabel!
    @IBOutlet weak var profileMedicineAddToCart: UIButton!
    @IBOutlet weak var profileMedicineCounter: UIStepper!
    @IBOutlet weak var profileMedicineCount: UILabel!
    @IBOutlet weak var profileMedicinePrescription: UIButton!
    
    var currentMID: String!

    let db = Firestore.firestore()
    var currentItem: Item! {
        didSet {
            
            let url = URL.init(string: (self.currentItem.images?.first)!)
            if url != nil {
                Nuke.loadImage(with: url!, into: self.profileMedicineImage)
            } else {
                //Nuke.loadImage(with: URL.init(string: AppConstants.GVS_DEFAULT_PROFILE_IMAGE_URL)!, into: self.profileMedicineImage)
                Nuke.loadImage(with: URL.init(string: "https://firebasestorage.googleapis.com/v0/b/farmacia-gvs.appspot.com/o/static%2Fmedicamentos.jpg?alt=media&token=44c1aa7b-596a-44bc-991f-b0d6434ec7b8")!, into: self.profileMedicineImage)
            }
            
            if self.currentItem.text == "" {
                profileMedicineDescription.text = ""
            } else {
                profileMedicineDescription.text = self.currentItem.text
            }
            
            self.profileMedicineName.text = self.currentItem?.name
            self.profileMedicineTag.text = "\(NSLocalizedString("Unit price", comment: "")): $\(String(self.currentItem.price))"
            //profileMedicineCount.text = "Cantitdad: \(Int(profileMedicineCounter.value)) $ \(String(self.currentItem.price))"
            profileMedicineCount.text = "\(NSLocalizedString("Quantity", comment: "")): \(Int(profileMedicineCounter.value))"
            
            profileMedicinePrescription.isHidden = (currentItem.prescription) ? false : true
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        initViews()
        medicineProfileConstraints()
    }
    
    private func initViews() {
        let query = db.collection(AppConstants.FIREBASE_DATABASE_TABLE_ITEMS).whereField("id", isEqualTo: currentMID)
        query.getDocuments { (snapshot, error) in
            if error != nil {
            } else {
                self.currentItem = Item(dictionary: snapshot?.documents.first?.data())
            }
        }
        
        self.title = NSLocalizedString("Product", comment: "")
        
        profileMedicineAddToCart.setImage(#imageLiteral(resourceName: "Cart"), for: .normal)
        profileMedicineAddToCart.setTitle(NSLocalizedString("Add to cart", comment: ""), for: .normal)
        profileMedicineAddToCart.tintColor = UIColor(named: "mainBlueColor")
        
        profileMedicineName.numberOfLines = 10
        profileMedicineTag.numberOfLines = 2
        
        profileMedicineDescription.isEditable = false
        
        profileMedicinePrescription.setTitle(NSLocalizedString("This medicine needs prescription", comment: ""), for: .normal)
        profileMedicinePrescription.setImage(#imageLiteral(resourceName: "Prescription"), for: .normal)
        profileMedicinePrescription.tintColor = UIColor.orange
        profileMedicinePrescription.isUserInteractionEnabled = false
        
        profileMedicineCounter.wraps = true
        profileMedicineCounter.autorepeat = true
        profileMedicineCounter.minimumValue = 1
        profileMedicineCounter.maximumValue = 5
        
    }
    
    private func medicineProfileConstraints() {
        view.addSubview(profileMedicineImage)
        view.addConstraintWithVisualFormat(format: "H:|-16-[v0(160)]", views: profileMedicineImage)
        view.addConstraintWithVisualFormat(format: "V:|-50-[v0(160)]", views: profileMedicineImage)
        
        view.addSubview(profileMedicineName)
        view.addConstraintWithVisualFormat(format: "H:|-200-[v0]-8-|", views: profileMedicineName)
        view.addConstraintWithVisualFormat(format: "V:|-80-[v0]", views: profileMedicineName)
        
        view.addSubview(profileMedicineDescription)
        view.addConstraintWithVisualFormat(format: "H:|-8-[v0]-8-|", views: profileMedicineDescription)
        view.addConstraintWithVisualFormat(format: "V:|-240-[v0]-340-|", views: profileMedicineDescription)
        
        view.addSubview(profileMedicinePrescription)
        view.addConstraintWithVisualFormat(format: "H:|-8-[v0]", views: profileMedicinePrescription)
        view.addConstraintWithVisualFormat(format: "V:|-380-[v0]", views: profileMedicinePrescription)
        
        view.addSubview(profileMedicineTag)
        view.addConstraintWithVisualFormat(format: "H:|-8-[v0]", views: profileMedicineTag)
        view.addConstraintWithVisualFormat(format: "V:|-420-[v0]", views: profileMedicineTag)
        
        view.addSubview(profileMedicineCounter)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]", views: profileMedicineCounter)
        view.addConstraintWithVisualFormat(format: "V:|-520-[v0]", views: profileMedicineCounter)
        
        view.addSubview(profileMedicineCount)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-120-|", views: profileMedicineCount)
        view.addConstraintWithVisualFormat(format: "V:|-480-[v0]", views: profileMedicineCount)
        
        view.addSubview(profileMedicineAddToCart)
        view.addConstraintWithVisualFormat(format: "H:|-120-[v0(160)]-120-|", views: profileMedicineAddToCart)
        view.addConstraintWithVisualFormat(format: "V:|-560-[v0(160)]", views: profileMedicineAddToCart)
    }
    @IBAction func increaseQuantity(_ sender: UIStepper) {
        //profileMedicineCount.text = "Cantitdad: \(Int(profileMedicineCounter.value)) $\(Double(sender.value * currentItem.price).description)"
        profileMedicineCount.text = "\(NSLocalizedString("Quantity", comment: "")): \(Int(profileMedicineCounter.value))"
        
    }
    
    @IBAction func addedToCart(_ sender: UIButton) {
        
        for i in 1...Int(profileMedicineCounter!.value) {
            print(i)
            AppDelegate.currentCart.items.append(currentItem)
            AppDelegate.publishCartChanges()
        }
        showToast(message: NSLocalizedString("The item was added to the cart", comment: ""))
    }
}
