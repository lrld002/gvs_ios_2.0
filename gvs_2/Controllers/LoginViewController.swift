//
//  LoginViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 26/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController{
    
    @IBOutlet weak var loginLogoImage: UIImageView!
    @IBOutlet weak var loginRegisterButton: UIButton!
    @IBOutlet weak var loginSignInButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
        setUpConstraints()
    }
    
    private func setUpViews() {
        
        loginLogoImage.image = UIImage(named: "fullLogo")
        loginLogoImage.contentMode = .scaleAspectFit
        
        loginRegisterButton.setTitle(NSLocalizedString("Sign up", comment: ""), for: .normal)
        loginRegisterButton.contentEdgeInsets = UIEdgeInsets(top: 4.0, left: 4.0, bottom: 4.0, right: 8.0)
        loginRegisterButton.layer.cornerRadius = 15.0
        loginRegisterButton.clipsToBounds = true
        loginRegisterButton.setTitleColor(UIColor.white, for: .normal)
        loginRegisterButton.backgroundColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        
        loginSignInButton.setTitle(NSLocalizedString("Log in", comment: ""), for: .normal)
        loginSignInButton.contentEdgeInsets = UIEdgeInsets(top: 4.0, left: 4.0, bottom: 4.0, right: 8.0)
        loginSignInButton.layer.cornerRadius = 15.0
        loginSignInButton.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        loginSignInButton.layer.borderWidth = 1
        loginSignInButton.clipsToBounds = true
        loginSignInButton.setTitleColor(#colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1), for: .normal)
        loginSignInButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    private func setUpConstraints() {
        
        view.addSubview(loginLogoImage)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: loginLogoImage)
        view.addConstraintWithVisualFormat(format: "V:|-(-80)-[v0]", views: loginLogoImage)
        
        view.addSubview(loginRegisterButton)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: loginRegisterButton)
        view.addConstraintWithVisualFormat(format: "V:|-320-[v0(48)]", views: loginRegisterButton)
        
        view.addSubview(loginSignInButton)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: loginSignInButton)
        view.addConstraintWithVisualFormat(format: "V:|-400-[v0(48)]", views: loginSignInButton)
    }
    
}
