//
//  SignUpViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 04/03/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameSignUp: UITextField!
    @IBOutlet weak var nameLimitSignUp: UILabel!
    @IBOutlet weak var emailSignUp: UITextField!
    @IBOutlet weak var passwordSignUp: UITextField!
    @IBOutlet weak var registerSignUp: UIButton!
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
        setUpConstraints()
    }
    
    private func setUpViews() {
        self.title = NSLocalizedString("Sign up", comment: "")
        
        nameSignUp.placeholder = NSLocalizedString("Complete name", comment: "")
        nameSignUp.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: nameSignUp.frame.height))
        nameSignUp.leftViewMode = .always
        nameSignUp.font = UIFont.systemFont(ofSize: 24)
        nameSignUp.backgroundColor = UIColor.clear
        nameSignUp.clipsToBounds = true
        nameSignUp.autocorrectionType = .no
        nameSignUp.autocapitalizationType = .words
        nameSignUp.layer.cornerRadius = 15.0
        nameSignUp.layer.borderWidth = 1
        nameSignUp.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        nameSignUp.isUserInteractionEnabled = true
        nameSignUp.delegate = self
        nameSignUp.tag = 2
        nameSignUp.returnKeyType = .next
        let nameGesture = UITapGestureRecognizer(target: self, action: #selector(editUserName))
        nameSignUp.addGestureRecognizer(nameGesture)
        
        nameLimitSignUp.isHidden = true
        nameLimitSignUp.textColor = #colorLiteral(red: 0.5647058824, green: 0.5647058824, blue: 0.5647058824, alpha: 1)
        nameLimitSignUp.text = String(15)
        
        emailSignUp.placeholder = NSLocalizedString("Email", comment: "")
        emailSignUp.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: emailSignUp.frame.height))
        emailSignUp.leftViewMode = .always
        emailSignUp.font = UIFont.systemFont(ofSize: 24)
        emailSignUp.backgroundColor = UIColor.clear
        emailSignUp.autocorrectionType = .no
        emailSignUp.keyboardType = .emailAddress
        emailSignUp.autocapitalizationType = .none
        emailSignUp.clipsToBounds = true
        emailSignUp.layer.cornerRadius = 15.0
        emailSignUp.layer.borderWidth = 1
        emailSignUp.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        //emailSignUp.delegate = self
        emailSignUp.tag = 0
        emailSignUp.returnKeyType = .next
        
        passwordSignUp.placeholder = NSLocalizedString("Password", comment: "")
        passwordSignUp.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: passwordSignUp.frame.height))
        passwordSignUp.leftViewMode = .always
        passwordSignUp.font = UIFont.systemFont(ofSize: 24)
        passwordSignUp.backgroundColor = UIColor.clear
        passwordSignUp.clipsToBounds = true
        passwordSignUp.isSecureTextEntry = true
        passwordSignUp.autocorrectionType = .no
        passwordSignUp.autocapitalizationType = .none
        passwordSignUp.layer.cornerRadius = 15.0
        passwordSignUp.layer.borderWidth = 1
        passwordSignUp.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        //passwordSignUp.delegate = self
        passwordSignUp.tag = 1
        passwordSignUp.returnKeyType = .next
        
        registerSignUp.setTitle(NSLocalizedString("Sign up", comment: ""), for: .normal)
        registerSignUp.contentEdgeInsets = UIEdgeInsets(top: 4.0, left: 4.0, bottom: 4.0, right: 8.0)
        registerSignUp.layer.cornerRadius = 15.0
        registerSignUp.clipsToBounds = true
        registerSignUp.setTitleColor(UIColor.white, for: .normal)
        registerSignUp.backgroundColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        let registerGesture = UITapGestureRecognizer(target: self, action: #selector(registerUser))
        registerSignUp.addGestureRecognizer(registerGesture)
    }
    
    private func setUpConstraints() {
        view.addSubview(nameSignUp)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: nameSignUp)
        view.addConstraintWithVisualFormat(format: "V:|-80-[v0]", views: nameSignUp)
        
        view.addSubview(nameLimitSignUp)
        view.addConstraintWithVisualFormat(format: "H:[v0]-80-|", views: nameLimitSignUp)
        view.addConstraintWithVisualFormat(format: "V:|-88-[v0]", views: nameLimitSignUp)
        
        view.addSubview(emailSignUp)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: emailSignUp)
        view.addConstraintWithVisualFormat(format: "V:|-120-[v0]", views: emailSignUp)
        
        view.addSubview(passwordSignUp)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: passwordSignUp)
        view.addConstraintWithVisualFormat(format: "V:|-160-[v0]", views: passwordSignUp)
        
        view.addSubview(registerSignUp)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: registerSignUp)
        view.addConstraintWithVisualFormat(format: "V:|-320-[v0(32)]", views: registerSignUp)
    }
    
    @objc func registerUser() {
        let register = Auth.auth()
        let email = emailSignUp.text
        let pass = passwordSignUp.text
        let name  = nameSignUp.text
        
        let credential = EmailAuthProvider.credential(withEmail: email!, password: pass!)
        
        Auth.auth().currentUser?.linkAndRetrieveData(with: credential) { (authResult, error) in
            if email != nil && pass != nil && self.nameSignUp.text != nil {
            self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_USERS).document(AppDelegate.currentUser.id).setData(["mail": email!,], merge: true)
            self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_USERS).document(AppDelegate.currentUser.id).setData(["name":name!,], merge: true)
                
                register.signIn(withEmail: email!, password: pass!, completion: { (result, error) in
                    if error != nil {
                        print(error!)
                    } else {
                        if register.currentUser != nil {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainNC")
                            self.present(initialViewController, animated: true, completion: nil)
                        }
                    }
                })
            }
        }
    }
    
    @objc func editUserName() {
        nameLimitSignUp.isHidden = false
        nameSignUp.becomeFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength: Int = 15
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        nameLimitSignUp.text = String(maxLength - newString.length)
        if ((maxLength - newString.length) == -1) {
            nameLimitSignUp.text = String(0)
        }
        return newString.length <= maxLength
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }

}
