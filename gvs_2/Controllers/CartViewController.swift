//
//  2ViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 02/02/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import Firebase
import Nuke

class CartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var cartPayButton: UIButton!
    
    var cartMID: String?
    var totalToResume = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
        setUpConstraints()
    }
    
    private func setUpViews() {
        self.title = NSLocalizedString("Shopping cart", comment: "")
        
        cartTableView.dataSource = self
        cartTableView.delegate = self
        
        cartPayButton.backgroundColor = UIColor.white
        cartPayButton.setTitle(NSLocalizedString("Continue", comment: ""), for: .normal)
        cartPayButton.setImage(#imageLiteral(resourceName: "Shopping"), for: .normal)
        cartPayButton.tintColor = UIColor(named: "mainBlueColor")
        let cartGesture = UITapGestureRecognizer(target: self, action: #selector(goToMap))
        cartPayButton.addGestureRecognizer(cartGesture)
    }
    
    private func setUpConstraints() {
        
        view.addSubview(cartTableView)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: cartTableView)
        view.addConstraintWithVisualFormat(format: "V:|-[v0]-60-|", views: cartTableView)
        
        view.addSubview(cartPayButton)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: cartPayButton)
        view.addConstraintWithVisualFormat(format: "V:[v0(60)]-|", views: cartPayButton)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var totalCalculated = 0.0
        
        for item in AppDelegate.currentCart.items {
            totalCalculated += Double(item.price)
        }
        print("El total es \(totalCalculated)")
        totalToResume = totalCalculated
        
        if AppDelegate.currentCart.items.count == 0 {
            cartPayButton.isEnabled = false
        }
        
        return AppDelegate.currentCart.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cart_cell", for: indexPath)
        
        let item = AppDelegate.currentCart.items[indexPath.row]
        
        cell.textLabel?.text = item.name
        cell.textLabel?.numberOfLines = 3
        cell.selectionStyle = .none
        
        let url = URL.init(string: (item.images?.first)!)
        if (url != nil && !url!.absoluteString.isEmpty) {
            Nuke.loadImage(with: url!, into: (cell.imageView)!)
        } else {
            Nuke.loadImage(with: URL(string: AppConstants.GVS_DEFAULT_PROFILE_IMAGE_URL)!, into: (cell.imageView)!)
        }
        
        cell.addSubview(cell.imageView!)
        cell.addConstraintWithVisualFormat(format: "H:|-8-[v0(70)]", views: cell.imageView!)
        cell.addConstraintWithVisualFormat(format: "V:|-8-[v0(70)]-8-|", views: cell.imageView!)
        
        cell.addSubview(cell.textLabel!)
        cell.addConstraintWithVisualFormat(format: "H:|-120-[v0]-8-|", views: cell.textLabel!)
        cell.addConstraintWithVisualFormat(format: "V:|-8-[v0]", views: cell.textLabel!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        /*let moreRowAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "More", handler:{action, indexpath in
            print("MORE•ACTION");
        });
        moreRowAction.backgroundColor = UIColor(red: 0.298, green: 0.851, blue: 0.3922, alpha: 1.0);*/
        
        let deleteRowAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: NSLocalizedString("Delete", comment: ""), handler:{action, indexpath in
            print("DELETE•ACTION")
            AppDelegate.currentCart.items.remove(at: indexPath.item)
            AppDelegate.publishCartChanges()
            self.cartTableView.deleteRows(at: [indexPath], with: .automatic)
        })
        
        //return [deleteRowAction, moreRowAction];
        return [deleteRowAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = AppDelegate.currentCart.items[indexPath.row]
        
        cartMID = item.id
        self.performSegue(withIdentifier: "show_cart_medicine", sender: self)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier ==  "show_cart_map" {
            let goMap = segue.destination as! LocationViewController
            goMap.totalArticles = String(AppDelegate.currentCart.items.count)
            goMap.totalPayment = totalToResume
            
        }
        if segue.identifier == "show_cart_medicine" {
            let goMedicine = segue.destination as! MedicineViewController
            goMedicine.currentMID = self.cartMID
        }
    }
    
    @objc func goToMap() {
        if (Auth.auth().currentUser?.isAnonymous)! {
            self.performSegue(withIdentifier: "show_cart_login", sender: self)
        } else {
            self.performSegue(withIdentifier: "show_cart_map", sender: self)
        }
    }

}
