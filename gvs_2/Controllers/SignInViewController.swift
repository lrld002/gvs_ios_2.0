//
//  SignInViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 05/03/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit

class SignInViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var signInEmail: UITextField!
    @IBOutlet weak var signInPassword: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signInRecoverPassword: UILabel!
    
    @objc private let facebookLoginButton = FBSDKLoginButton()
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
        setUpConstraints()
    }
    
    private func setUpViews() {
        self.title = NSLocalizedString("Log in", comment: "")
        
        signInEmail.placeholder = NSLocalizedString("Email", comment: "")
        signInEmail.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: signInEmail.frame.height))
        signInEmail.leftViewMode = .always
        signInEmail.font = UIFont.systemFont(ofSize: 24)
        signInEmail.backgroundColor = UIColor.clear
        signInEmail.keyboardType = .emailAddress
        signInEmail.autocorrectionType = .no
        signInEmail.autocapitalizationType = .none
        signInEmail.clipsToBounds = true
        signInEmail.layer.cornerRadius = 15.0
        signInEmail.layer.borderWidth = 1
        signInEmail.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        
        signInPassword.placeholder = NSLocalizedString("Password", comment: "")
        signInPassword.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: signInPassword.frame.height))
        signInPassword.leftViewMode = .always
        signInPassword.font = UIFont.systemFont(ofSize: 24)
        signInPassword.backgroundColor = UIColor.clear
        signInPassword.isSecureTextEntry = true
        signInPassword.autocorrectionType = .no
        signInPassword.autocapitalizationType = .none
        signInPassword.clipsToBounds = true
        signInPassword.layer.cornerRadius = 15.0
        signInPassword.layer.borderWidth = 1
        signInPassword.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        
        signInButton.setTitle(NSLocalizedString("Log in", comment: ""), for: .normal)
        signInButton.contentEdgeInsets = UIEdgeInsets(top: 4.0, left: 4.0, bottom: 4.0, right: 8.0)
        signInButton.layer.cornerRadius = 15.0
        signInButton.clipsToBounds = true
        signInButton.setTitleColor(UIColor.white, for: .normal)
        signInButton.backgroundColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        
        facebookLoginButton.delegate = self
        facebookLoginButton.contentVerticalAlignment = .center
        facebookLoginButton.layer.cornerRadius = 20.0
        facebookLoginButton.clipsToBounds = true
        
        signInRecoverPassword.font = UIFont.systemFont(ofSize: 12)
        signInRecoverPassword.text = NSLocalizedString("Forgot your password?", comment: "")
        signInRecoverPassword.textColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        signInRecoverPassword.textAlignment = .center
        signInRecoverPassword.isUserInteractionEnabled = true
        let recoverPass = UITapGestureRecognizer(target: self, action: #selector(passRecovery))
        signInRecoverPassword.addGestureRecognizer(recoverPass)
    }
    
    private func setUpConstraints() {
        view.addSubview(signInEmail)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: signInEmail)
        view.addConstraintWithVisualFormat(format: "V:|-120-[v0]", views: signInEmail)
        
        view.addSubview(signInPassword)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: signInPassword)
        view.addConstraintWithVisualFormat(format: "V:|-180-[v0]", views: signInPassword)
        
        view.addSubview(signInButton)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: signInButton)
        view.addConstraintWithVisualFormat(format: "V:|-240-[v0(48)]", views: signInButton)
        
        view.addSubview(facebookLoginButton)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: facebookLoginButton)
        view.addConstraintWithVisualFormat(format: "V:|-320-[v0(48)]", views: facebookLoginButton)
        
        view.addSubview(signInRecoverPassword)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: signInRecoverPassword)
        view.addConstraintWithVisualFormat(format: "V:[v0]-240-|", views: signInRecoverPassword)
    }
    
    @objc func signIn() {
        let user = signInEmail.text
        let pass = signInPassword.text
        
        let auth = Auth.auth()
        if user != nil && pass != nil {
            try! auth.signOut()
            auth.signIn(withEmail: user!, password: pass!) { (result, error) in
                if error != nil {
                    print(error!)
                    let alert = UIAlertController(title: NSLocalizedString("Please, try again", comment: ""), message: NSLocalizedString("Verify your email and password", comment: ""), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainNC")
                    self.present(initialViewController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            return
        }
        if result.isCancelled {
            return
        }
        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
        Auth.auth().currentUser!.linkAndRetrieveData(with: credential) { (authResult, error) in
            
            Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                if error != nil {
                    return
                }
                
                let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "email,name,picture"], tokenString: result.token.tokenString, version: nil, httpMethod: "GET")
                
                req?.start(completionHandler: { (connection, result, error ) -> Void in
                    if(error == nil)
                    {
                        if let data = result as? [String: Any] {
                            
                            //if let firebaseUser = Auth.auth().currentUser {
                            self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_USERS).document(Auth.auth().currentUser?.uid ?? "").addSnapshotListener { (snapshot, error) in
                                
                                //if error == nil || snapshot?.data()?.isEmpty ?? true{
                                if error == nil {
                                    /*AppDelegate.currentUser = User(id: firebaseUser.uid,
                                     payId: "",
                                     mail: data["email"] as! String,
                                     name: (data["name"] as? String) ?? "Anon",
                                     photoUrl: (data["picture"] as? String) ?? "",
                                     gender: false,
                                     timestamp: Int64(NSDate().timeIntervalSince1970  * 1000),
                                     //timestamp: 1,
                                     cards: [])*/
                                    
                                    self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_USERS).document(AppDelegate.currentUser.id).setData(["mail": data["email"] as! String,], merge: true)
                                    
                                    self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_USERS).document(AppDelegate.currentUser.id).setData(["name": (data["name"] as? String) ?? "Anon",], merge: true)
                                    
                                    self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_USERS).document(AppDelegate.currentUser.id).setData(["photoUrl": (data["picture"] as? String) ?? "",], merge: true)
                                    print(AppDelegate.currentUser.toData())
                                } else {
                                    AppDelegate.currentUser = User(dictionary: snapshot?.data())
                                }
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainNC")
                                self.present(initialViewController, animated: true, completion: nil)
                            }
                            //}
                        }
                    }
                })
            }
            if let error = error {
                print(error.localizedDescription)
                return
            }
        }
        
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        FBSDKLoginManager().logOut()
    }
    
    @objc func passRecovery(_label: UILabel) {
        let alert = UIAlertController(title: NSLocalizedString("Forgot your password?", comment: ""), message: NSLocalizedString("Please enter your email address", comment: ""), preferredStyle: .alert)
        alert.addTextField {
            textField in
            textField.placeholder = NSLocalizedString("Email", comment: "")
            textField.keyboardType = .emailAddress
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            
        }
        alert.addAction(UIAlertAction(title: NSLocalizedString("Send", comment: ""), style: .default, handler: {(alert1: UIAlertAction!) in
            let resetEmail = alert.textFields?.first?.text
            Auth.auth().sendPasswordReset(withEmail: resetEmail!, completion: {error in
                if error != nil {
                    let alert = UIAlertController(title: NSLocalizedString("Please, try again", comment: ""), message: NSLocalizedString("We could not start the restoration process", comment: ""), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(title: NSLocalizedString("Done", comment: ""), message: NSLocalizedString("Check your inbox to continue with the re-establishment process", comment: ""), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            })
            
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }

}
