//
//  ResumeViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 30/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import Firebase
import Nuke

class ResumeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var resumeTotalArticles: UILabel!
    @IBOutlet weak var resumePayButton: UIButton!
    @IBOutlet weak var resumeAddLocation: UIButton!
    @IBOutlet weak var resumeTableView: UITableView!
    
    let db = Firestore.firestore()
    
    var totalArticles: String!
    var totalPayment: Double!
    var selectedPosition: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
        setUpConstraints()
    }
    
    private func setUpViews() {
        self.title = "Resumen de pedido"
        
        resumeTableView.dataSource = self
        resumeTableView.delegate = self
        
        resumeTotalArticles.text = "\(totalArticles ?? String(0)) Productos \n Total a pagar: $\(String(totalPayment))"
        resumeTotalArticles.numberOfLines = 2
        resumeTotalArticles.textColor = UIColor(named: "mainBlueColor")
        resumeTotalArticles.backgroundColor = UIColor.white
        resumeTotalArticles.textAlignment = .center
        resumeTotalArticles.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        resumeTotalArticles.layer.borderWidth = 1
        
        resumeAddLocation.setTitle(selectedPosition, for: .normal)
        resumeAddLocation.setImage(#imageLiteral(resourceName: "Location"), for: .normal)
        resumeAddLocation.tintColor = UIColor(named: "mainBlueColor")
        resumeAddLocation.backgroundColor = UIColor.white
        resumeAddLocation.titleLabel?.numberOfLines = 2
        resumeAddLocation.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        resumeAddLocation.layer.borderWidth = 1
        
        resumePayButton.setTitle("Realizar pedido", for: .normal)
        resumePayButton.setImage(#imageLiteral(resourceName: "Shopping"), for: .normal)
        resumePayButton.tintColor = UIColor(named: "mainBlueColor")
        resumePayButton.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        resumePayButton.layer.borderWidth = 1
        let payGesture = UITapGestureRecognizer(target: self, action: #selector(realizePayment))
        resumePayButton.addGestureRecognizer(payGesture)
    }
    
    private func setUpConstraints() {
        view.addSubview(resumeTableView)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: resumeTableView)
        view.addConstraintWithVisualFormat(format: "V:|-[v0]-140-|", views: resumeTableView)
        
        view.addSubview(resumeTotalArticles)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: resumeTotalArticles)
        view.addConstraintWithVisualFormat(format: "V:[v0]-120-|", views: resumeTotalArticles)
        
        view.addSubview(resumeAddLocation)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: resumeAddLocation)
        view.addConstraintWithVisualFormat(format: "V:[v0(60)]-60-|", views: resumeAddLocation)
        
        view.addSubview(resumePayButton)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: resumePayButton)
        view.addConstraintWithVisualFormat(format: "V:[v0(60)]-|", views: resumePayButton)
    }
    
    @objc func realizePayment() {
        
        AppDelegate.currentOrder = Order(id: AppDelegate.currentUser.id ,
                          user: AppDelegate.currentUser,
                          items: AppDelegate.currentCart.items,
                          location: selectedPosition!,
                          totalPayment: totalPayment,
                          timestamp: Int64(NSDate().timeIntervalSince1970  * 1000))
        
        let oID = db.collection(AppConstants.FIREBASE_DATABASE_TABLE_ORDERS).document().documentID
        db.collection(AppConstants.FIREBASE_DATABASE_TABLE_ORDERS).document(oID).setData(AppDelegate.currentOrder.toData())
        db.collection(AppConstants.FIREBASE_DATABASE_TABLE_ORDERS).document(oID).setData(["oid": String(oID),], merge: true)
        
        print(AppDelegate.currentOrder.toData())
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppDelegate.currentCart.items.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resume_cell", for: indexPath)
        let item = AppDelegate.currentCart.items[indexPath.row]
        
        cell.textLabel?.text = item.name
        cell.textLabel?.numberOfLines = 3
        cell.selectionStyle = .none
        
        let url = URL.init(string: (item.images?.first)!)
        if (url != nil && !url!.absoluteString.isEmpty) {
            Nuke.loadImage(with: url!, into: (cell.imageView)!)
        } else {
            Nuke.loadImage(with: URL(string: AppConstants.GVS_DEFAULT_PROFILE_IMAGE_URL)!, into: (cell.imageView)!)
        }
        
        cell.addSubview(cell.imageView!)
        cell.addConstraintWithVisualFormat(format: "H:|-8-[v0(70)]", views: cell.imageView!)
        cell.addConstraintWithVisualFormat(format: "V:|-8-[v0(70)]-8-|", views: cell.imageView!)
        
        cell.addSubview(cell.textLabel!)
        cell.addConstraintWithVisualFormat(format: "H:|-120-[v0]-8-|", views: cell.textLabel!)
        cell.addConstraintWithVisualFormat(format: "V:|-8-[v0]", views: cell.textLabel!)
        
        return cell
    }

}
