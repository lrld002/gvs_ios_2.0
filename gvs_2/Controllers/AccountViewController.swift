//
//  AccountViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 26/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit

class AccountViewController: UIViewController {

    @IBOutlet weak var accountOrdersButton: UIButton!
    @IBOutlet weak var accountPaymethodsButton: UIButton!
    @IBOutlet weak var accountConfiguration: UIButton!
    @IBOutlet weak var accountAbout: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
        setUpConstraints()
        
    }
    
    private func setUpViews() {
        self.title = NSLocalizedString("My account", comment: "")
        
        accountOrdersButton.setImage(#imageLiteral(resourceName: "Shopping"), for: .normal)
        accountOrdersButton.setTitle(NSLocalizedString("Orders placed", comment: ""), for: .normal)
        accountOrdersButton.tintColor = UIColor.init(named: "mainBlueColor")
        accountOrdersButton.contentHorizontalAlignment = .left
        accountOrdersButton.contentVerticalAlignment = .center
        accountOrdersButton.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        accountOrdersButton.layer.borderWidth = 1
        
        accountPaymethodsButton.setImage(#imageLiteral(resourceName: "Card"), for: .normal)
        accountPaymethodsButton.setTitle(NSLocalizedString("Payment methods", comment: ""), for: .normal)
        accountPaymethodsButton.tintColor = UIColor.init(named: "mainBlueColor")
        accountPaymethodsButton.contentHorizontalAlignment = .left
        accountPaymethodsButton.contentVerticalAlignment = .center
        accountPaymethodsButton.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        accountPaymethodsButton.layer.borderWidth = 1
        
        accountConfiguration.setImage(#imageLiteral(resourceName: "Configuration"), for: .normal)
        accountConfiguration.setTitle(NSLocalizedString("Configuration", comment: ""), for: .normal)
        accountConfiguration.tintColor = UIColor.init(named: "mainBlueColor")
        accountConfiguration.contentHorizontalAlignment = .left
        accountConfiguration.contentVerticalAlignment = .center
        accountConfiguration.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        accountConfiguration.layer.borderWidth = 1
        
        accountAbout.setTitle(NSLocalizedString("About us", comment: ""), for: .normal)
        accountAbout.tintColor = UIColor.init(named: "mainBlueColor")
        accountAbout.contentHorizontalAlignment = .left
        accountAbout.contentVerticalAlignment = .center
        accountAbout.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        accountAbout.layer.borderWidth = 1
    }
    
    private func setUpConstraints() {
        
        view.addSubview(accountOrdersButton)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: accountOrdersButton)
        view.addConstraintWithVisualFormat(format: "V:|-24-[v0(48)]", views: accountOrdersButton)
        
        view.addSubview(accountPaymethodsButton)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: accountPaymethodsButton)
        view.addConstraintWithVisualFormat(format: "V:|-80-[v0(48)]", views: accountPaymethodsButton)
        
        view.addSubview(accountConfiguration)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: accountConfiguration)
        view.addConstraintWithVisualFormat(format: "V:|-136-[v0(48)]", views: accountConfiguration)
        
        view.addSubview(accountAbout)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: accountAbout)
        view.addConstraintWithVisualFormat(format: "V:|-360-[v0(48)]", views: accountAbout)
    }

}
