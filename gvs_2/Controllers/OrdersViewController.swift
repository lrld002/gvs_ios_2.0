//
//  OrdersViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 30/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit
import Firebase

class OrdersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var ordersTableView: UITableView!
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
        setUpConstraints()
    }
    
    private func setUpViews() {
        self.title = NSLocalizedString("Orders placed", comment: "")
        
        ordersTableView.dataSource = self
        ordersTableView.delegate = self
        
        /*db.collection(AppConstants.FIREBASE_DATABASE_TABLE_ORDERS).document().addSnapshotListener { (snapshot, error) in
            if snapshot != nil {
                snapshot.
            }
        }*/
    }
    
    private func setUpConstraints() {
        view.addSubview(ordersTableView)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: ordersTableView)
        view.addConstraintWithVisualFormat(format: "V:|-[v0]-60-|", views: ordersTableView)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orders_cell", for: indexPath)
        
        //let orderDate = AppDelegate.currentOrder.timestamp
        
        if var timeResult = AppDelegate.currentOrder.timestamp{
            timeResult = timeResult / 1000
            let date = NSDate(timeIntervalSince1970: TimeInterval(timeResult))
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let localDate = dateFormatter.string(from: date as Date)
            //self.postTimeStamp.text = localDate
            
            cell.textLabel?.text = localDate
        }
        
        return cell
    }

}
