//
//  PaymentMethodsViewController.swift
//  gvs_2
//
//  Created by Gerardo García on 30/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit

class PaymentMethodsViewController: UIViewController {

    @IBOutlet weak var addedCardFromUser: UIButton!
    @IBOutlet weak var addPaymentMethodButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
        setUpConstraints()
    }
    
    private func setUpViews() {
        self.title = "Metodos de pago"
        
        addedCardFromUser.setImage(#imageLiteral(resourceName: "Card"), for: .normal)
        addedCardFromUser.setTitle(AppDelegate.currentUser.cards.first?.cardNumber, for: .normal)
        addedCardFromUser.tintColor = UIColor.init(named: "mainBlueColor")
        addedCardFromUser.contentHorizontalAlignment = .left
        addedCardFromUser.contentVerticalAlignment = .center
        addedCardFromUser.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        addedCardFromUser.layer.borderWidth = 1
        
        if AppDelegate.currentUser.cards.first?.cardNumber == nil {
            addedCardFromUser.isHidden = true
        } else {
            addedCardFromUser.isHidden = false
        }
        
        addPaymentMethodButton.setTitle(" Agregar metodo de pago", for: .normal)
        addPaymentMethodButton.tintColor = UIColor.init(named: "mainBlueColor")
        addPaymentMethodButton.contentHorizontalAlignment = .left
        addPaymentMethodButton.contentVerticalAlignment = .center
        addPaymentMethodButton.layer.borderColor = #colorLiteral(red: 0.02745098039, green: 0.1647058824, blue: 0.2588235294, alpha: 1)
        addPaymentMethodButton.layer.borderWidth = 1
    }
    
    private func setUpConstraints() {
        
        view.addSubview(addedCardFromUser)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: addedCardFromUser)
        view.addConstraintWithVisualFormat(format: "V:|-80-[v0(48)]", views: addedCardFromUser)
        
        view.addSubview(addPaymentMethodButton)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: addPaymentMethodButton)
        view.addConstraintWithVisualFormat(format: "V:|-136-[v0(48)]", views: addPaymentMethodButton)
    }

}
