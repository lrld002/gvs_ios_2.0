//
//  User.swift
//  gvs_2
//
//  Created by Gerardo García on 26/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

class User : FirebaseObject {
    
    var id: String!
    var payId: String!
    var mail: String!
    var name: String!
    var photoUrl: String!
    var gender: Bool!
    var timestamp: Int64!
    var cards: [OpenpayCard] = [OpenpayCard]()
    
    init(){
        self.id = ""
        self.payId = ""
        self.mail = ""
        self.name = ""
        self.photoUrl = ""
        self.gender = false
        self.timestamp = Int64(NSDate().timeIntervalSince1970 * 1000)
    }
    
    init(id: String, payId: String, mail: String, name: String, photoUrl: String, gender: Bool, timestamp: Int64, cards: [OpenpayCard]) {
        self.id = id
        self.payId = payId
        self.mail = mail
        self.name = name
        self.photoUrl = photoUrl
        self.gender = gender
        self.timestamp = timestamp
        self.cards = cards
    }
    
    init(dictionary: [String: Any]?) {
        self.id = dictionary?["id"] as? String
        self.payId = dictionary?["payId"] as? String
        self.mail = dictionary?["mail"] as? String
        self.name = dictionary?["name"] as? String
        self.photoUrl = dictionary?["photoUrl"] as? String
        self.gender = dictionary?["gender"] as? Bool
        self.timestamp = dictionary?["timestamp"] as? Int64
        self.cards = cardsData(data: dictionary?["cards"] as? [Any])
    }
    
    private func cardsData(data: [Any]?) -> [OpenpayCard] {
        var cards = [OpenpayCard]()
        data?.forEach{ (card) in
            cards.append(OpenpayCard(dictionary: card as? [String: Any]))
        }
        return cards
    }
    
    private func cardsData() -> [Any] {
        var data = [Any]()
        cards.forEach { (card) in
            data.append(card.toData())
        }
        return data
    }
    
    func toData() -> [String : Any] {
        let data: [String: Any] = [
            "id" : id,
            "payId": payId,
            "mail" : mail,
            "name": name,
            "photoUrl": photoUrl,
            "gender": gender,
            "timestamp": timestamp,
            "cards": cardsData()
        ]
        return data
    }
}
