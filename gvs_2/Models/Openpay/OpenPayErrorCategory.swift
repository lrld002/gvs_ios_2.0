//
//  OpenPayErrorCategory.swift
//  gvs_2
//
//  Created by Alain Peralta on 2/1/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

enum OpenPayErrorCategory : String, Decodable {
    case Request
    case Internal
    case Gateway
    
    enum CodingKeys : String, CodingKey {
        case Request = "request"
        case Internal = "internal"
        case Gateway = "gateway"
    }
    
}
