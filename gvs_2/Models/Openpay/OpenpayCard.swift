//
//  File.swift
//  gvs_2
//
//  Created by Alain Peralta on 2/1/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

struct OpenpayCard : FirebaseObject, Decodable {
    let type: String
    let brand: OpenpayCardBrand
    let address: OpenpayAddress
    let id: String
    let cardNumber: String
    let holderName: String
    let expirationYear: String
    let expirationMonth: String
    let allowsCharges: Bool
    let allowsPayouts: Bool
    let creationDate: String
    let bankName: String
    let customerId: String
    let pointsCard: Bool?
    
    init(dictionary: [String: Any]?) {
        self.type = dictionary?["type"] as! String
        self.brand = OpenpayCardBrand.init(rawValue: dictionary?["brand"] as! String) ?? OpenpayCardBrand.Unknown
        self.address = OpenpayAddress(dictionary: dictionary?["address"] as? [String : Any])
        self.id = dictionary?["id"] as! String
        self.cardNumber = dictionary?["cardNumber"] as! String
        self.holderName = dictionary?["holderName"] as! String
        self.expirationYear = dictionary?["expirationYear"] as! String
        self.expirationMonth = dictionary?["expirationMonth"] as! String
        self.allowsCharges = dictionary?["allowsCharges"] as! Bool
        self.allowsPayouts = dictionary?["allowsPayouts"] as! Bool
        self.creationDate = dictionary?["creationDate"] as! String
        self.bankName = dictionary?["bankName"] as! String
        self.customerId = dictionary?["customerId"] as! String
        self.pointsCard = dictionary?["pointsCard"] as? Bool
    }
    
    enum CodingKeys: String, CodingKey {
        case type = "type"
        case brand = "brand"
        case address = "address"
        case id = "id"
        case cardNumber = "card_number"
        case holderName = "holder_name"
        case expirationYear = "expiration_year"
        case expirationMonth = "expiration_month"
        case allowsCharges = "allows_charges"
        case allowsPayouts = "allows_payouts"
        case creationDate = "creation_date"
        case bankName = "bank_name"
        case customerId = "customer_id"
        case pointsCard = "points_card"
    }
    
    func toData() -> [String : Any] {
        let data = [
            "type": type,
            "brand": brand.rawValue,
            "address": address.toData(),
            "id": id,
            "cardNumber": cardNumber,
            "holderName": holderName,
            "expirationYear": expirationYear,
            "expirationMonth": expirationMonth,
            "allowsCharges": allowsCharges,
            "allowsPayouts": allowsPayouts,
            "creationDate": creationDate,
            "bankName": bankName,
            "customerId": customerId,
            "pointsCard": pointsCard ?? false
        ] as [String: Any]
        return data
    }
}
