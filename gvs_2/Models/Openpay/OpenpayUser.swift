//
//  OpenpayUser.swift
//  gvs_2
//
//  Created by Alain Peralta on 2/1/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

struct OpenpayUser : Decodable {
    let id: String
    let creationDate: String
    let name: String
    let lastName: String?
    let email: String
    let phoneNumber: String?
    let externalId: String?
    let status: String?
    let balance: Double?
    let clabe: String?
    let address: OpenpayAddress?
    let store: OpenpayStore?
    
    enum CodingKeys : String, CodingKey {
        case id = "id"
        case creationDate = "creation_date"
        case name = "name"
        case lastName = "last_name"
        case email = "email"
        case phoneNumber = "phone_number"
        case externalId = "external_id"
        case status = "status"
        case balance = "balance"
        case clabe = "clabe"
        case address = "address"
        case store = "store"
    }
}
