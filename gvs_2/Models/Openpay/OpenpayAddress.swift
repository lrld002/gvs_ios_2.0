//
//  OpenpayAddress.swift
//  gvs_2
//
//  Created by Alain Peralta on 2/1/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

struct OpenpayAddress: FirebaseObject, Decodable {
    let line1: String
    let line2: String
    let line3: String
    let postalCode: String
    let state: String
    let city: String
    let countryCode: String
    
    init(dictionary: [String: Any]?) {
        self.line1 = dictionary?["line1"] as? String ?? ""
        self.line2 = dictionary?["line2"] as? String ?? ""
        self.line3 = dictionary?["line3"] as? String ?? ""
        self.postalCode = dictionary?["postalCode"] as? String ?? ""
        self.state = dictionary?["state"] as? String ?? ""
        self.city = dictionary?["city"] as? String ?? ""
        self.countryCode = dictionary?["countryCode"] as? String ?? ""
    }
    
    enum CodingKeys: String, CodingKey {
        case line1 = "line1"
        case line2 = "line2"
        case line3 = "line3"
        case postalCode = "postal_code"
        case state = "state"
        case city = "city"
        case countryCode = "country_code"
    }
    
    func toData() -> [String : Any] {
        let data = [
            "line1": line1,
            "line2": line2,
            "line3": line3,
            "postalCode": postalCode,
            "state": state,
            "city": city,
            "countryCode": countryCode
            ] as [String: Any]
        return data
    }
}
