//
//  OpenpayPayment.swift
//  
//
//  Created by Alain Peralta on 2/5/19.
//

import Foundation

struct OpenpayPayment: Decodable {
    let id: String
    let amount: Double
    let authorization: String
    let method: String
    let operationType: String
    let transactionType: String
    let card: OpenpayCard
    let status: String
    let currency: String
    let exchangeRate: OpenpayExchangeRate
    let creationDate: String
    let operationDate: String
    let description: String
    let errorMessage: String?
    let orderId: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case amount = "amount"
        case authorization = "authorization"
        case method = "method"
        case operationType = "operation_type"
        case transactionType = "transaction_type"
        case card = "card"
        case status = "status"
        case currency = "currency"
        case exchangeRate = "exchange_rate"
        case creationDate = "creation_date"
        case operationDate = "operation_date"
        case description = "description"
        case errorMessage = "error_message"
        case orderId = "order_id"
    }
}
