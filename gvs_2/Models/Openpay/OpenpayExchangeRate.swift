//
//  OpenpayExchangeRate.swift
//  gvs_2
//
//  Created by Alain Peralta on 2/5/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

struct OpenpayExchangeRate : Decodable {
    let from: String
    let date: String
    let value: Double
    let to: String
}
