//
//  OpenpayError.swift
//  gvs_2
//
//  Created by Alain Peralta on 2/1/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

struct OpenpayError : Decodable {
    let category: OpenPayErrorCategory 
    let description: String
    let httpCode: Int
    let errorCode: Int
    let requestId: String
    let fraudRules: [String]
    
    enum CodingKeys : String, CodingKey {
        case category = "category"
        case description = "description"
        case httpCode = "http_code"
        case errorCode = "error_code"
        case requestId = "request_id"
        case fraudRules = "fraud_rules"
    }
    
}
