//
//  OpenpayStore.swift
//  gvs_2
//
//  Created by Alain Peralta on 2/1/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

struct OpenpayStore : Decodable {
    let reference: String
    let barcodeUrl: URL
    
    enum CodingKeys: String, CodingKey {
        case reference = "reference"
        case barcodeUrl = "barcode_url"
    }
}
