//
//  OpenpayCardBrand.swift
//  gvs_2
//
//  Created by Alain Peralta on 2/1/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

enum OpenpayCardBrand : String, Decodable {
    case Visa = "visa"
    case MasterCard = "mastercard"
    case AmericanExpress = "american_express"
    case Carnet = "carnet"
    case Unknown
}
