//
//  Resume.swift
//  gvs_2
//
//  Created by Gerardo García on 28/02/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation
import Firebase

class Order: FirebaseObject {
    
    var id: String!
    var user: User!
    var items: [Item]!
    var location: String!
    var totalPayment = 0.0
    var timestamp: Int64!
    //var orders: [Order]!
    
    init() {
        self.id = ""
        self.user = User()
        self.items = []
        self.location = ""
        self.timestamp = Int64(NSDate().timeIntervalSince1970 * 1000)
        //self.orders = []
    }
    
    init(id: String, user: User, items: [Item], location: String, totalPayment: Double, timestamp: Int64) {
        self.id = id
        self.user = user
        self.items = items
        self.location = location
        self.totalPayment = totalPayment
        self.timestamp = timestamp
        //self.orders = orders
    }
    
    init(dictionary: [String: Any]?) {
        self.id = dictionary?["id"] as? String
        self.user = dictionary?["user"] as? User
        self.items = itemsData(data: dictionary?["items"] as? [Any])
        self.location = dictionary?["location"] as? String
        self.totalPayment = dictionary?["totalPayment"] as? Double ?? 0.0
        self.timestamp = dictionary?["timestamp"] as? Int64
        //self.orders = ordersData(data: dictionary?["orders"] as? [Any])
    }
    
    func itemsData(data: [Any]?) -> [Item] {
        var items = [Item]()
        data?.forEach { (item) in
            items.append(Item(dictionary: item as? [String: Any]))
        }
        return items
    }
    
    func itemsData() -> [Any] {
        var data = [Any]()
        items.forEach { (item) in
            data.append(item.toData())
        }
        return data
    }
    
    /*func ordersData(data: [Any]?) -> [Order] {
        var orders = [Order]()
        data?.forEach { (order) in
            orders.append(Order(dictionary: order as? [String: Any]))
        }
        return orders
    }
    
    func ordersData() -> [Any] {
        var data = [Any]()
        orders.forEach { (order) in
            data.append(order.toData())
        }
        return data
    }*/
    
    func toData() -> [String : Any] {
        let data: [String: Any] = [
            "id" : id,
            "user" : user.toData(),
            "items" : itemsData(),
            "location" : location,
            "totalPaylemt" : totalPayment,
            "timestamp" : timestamp
            //,
            //"orders" : ordersData()
        ]
        return data
    }
    
}
