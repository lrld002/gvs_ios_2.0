//
//  ShoppingCart.swift
//  gvs_2
//
//  Created by Gerardo García on 26/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation
import FirebaseFirestore

class ShoppingCart : FirebaseObject{
    var id: String!
    var user: User!
    var items: [Item]!
    
    init() {
        self.id = ""
        self.user = User()
        self.items = []
    }
    
    init(id: String, user: User, items: [Item]) {
        self.id = id
        self.user = user
        self.items = items
    }
    
    init(dictionary: [String: Any]?) {
        self.id = dictionary?["id"] as? String
        self.user = dictionary?["user"] as? User
        self.items = itemsData(data: dictionary?["items"] as? [Any])
    }
    
    func itemsData(data: [Any]?) -> [Item] {
        var items = [Item]()
        data?.forEach { (item) in
            items.append(Item(dictionary: item as? [String: Any]))
        }
        return items
    }
    
    func itemsData() -> [Any] {
        var data = [Any]()
        items.forEach { (item) in
            data.append(item.toData())
        }
        return data
    }
    
    func toData() -> [String : Any] {
        let data = [
            "id": id,
            "user": user.toData(),
            "items": itemsData()
            ] as [String: Any]
        return data
    }
}
