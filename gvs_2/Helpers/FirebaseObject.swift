//
//  FirebaseObject.swift
//  gvs_2
//
//  Created by Gerardo García on 26/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

protocol FirebaseObject {
    func toData() -> [String: Any];
}
