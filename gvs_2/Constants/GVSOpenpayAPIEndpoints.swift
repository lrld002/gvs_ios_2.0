//
//  GVSOpenpayAPIEndpoints.swift
//  gvs_2
//
//  Created by Alain Peralta on 1/29/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

class GVSOpenpayAPIEndpoints {
    
    /**
     EndPoint para obtener o eliminar clientes
     ```
      clients/
     ```
 
      Respuesta
 
      GET
      Si el identificador existe regresa un objeto cliente con la
      información del cliente. Error en caso contrario
 
      DELETE
      Si el cliente se borra correctamente la respuesta es vacía,
      si no se puede borrar se regresa un objeto error indicando el motivo.
 
      Operaciones soportadas
      GET, DELETE
 
      Parametros
      :id
    */
    public static let CLIENTS = "pay/clients/"
    
    //
    //  clients/new/
    //
    //  Respuesta
    //  Un objeto cliente en caso que se hayan enviado todos los datos
    //  correctamente, o una respuesta de error si ocurrió algun problema
    //  en la creación.
    //
    //  Operaciones soportadas
    //  PUT
    //
    //  Parametros
    //  :email :name :requires_account
    //
    public static let CLIENTS_NEW = "pay/clients/new/"
    
    //
    //  cards/
    //
    //  Respuesta
    //  GET
    //  Regresa un objeto tarjeta
    //
    //  DELETE
    //  Si la tarjeta se borra correctamente la respuesta es vacía, si no se
    //  puede borrar se regresa un objeto error indicando el motivo.
    //
    //  Operaciones soportadas
    //  GET, DELETE
    //
    //  Parametros
    //  :idCliente :idTarjeta (Sólo DELETE)
    //
    public static let CARDS = "pay/cards/"
    
    //
    //  cards/new/
    //
    //  Respuesta
    //  Regresa un objeto tarjeta cuando se creó correctamente o una
    //  respuesta de error si ocurrió algún problema en la creación.
    //
    //  Operaciones soportadas
    //  GET, DELETE
    //
    //  Parametros
    //  :idCliente :deviceId :idTarjeta (Sólo DELETE)
    //
    public static let CARDS_NEW = "pay/cards/new/"
    
}
