//
//  FirebaseFunctions.swift
//  gvs_2
//
//  Created by Alain Peralta on 2/12/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

class GVSFirebaseFunctions {
    public static let NEW_CLIENT = "newClient"
    public static let NEW_CARD = "newCard"
    public static let TEST = "test"
}
