//
//  UITextField.swift
//  gvs_2
//
//  Created by Gerardo García on 04/03/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    func isEmailValid() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailText = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailText.evaluate(with: text)
    }
}
