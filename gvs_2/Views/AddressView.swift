//
//  AddressView.swift
//  gvs_2
//
//  Created by Alain Peralta on 2/5/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation
import UIKit

class AddressView: UIView {
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "AddressView", bundle: Bundle(for: AddressView.self)).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
}
