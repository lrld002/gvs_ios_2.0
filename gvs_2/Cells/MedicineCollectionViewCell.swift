//
//  MedicineCollectionViewCell.swift
//  gvs_2
//
//  Created by Gerardo García on 26/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit

class MedicineCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var medicineView: UIView!
    @IBOutlet weak var medicineImage: UIImageView!
    @IBOutlet weak var medicineName: UILabel!
    @IBOutlet weak var medicineTag: UILabel!
}
